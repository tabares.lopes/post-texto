# Estrutura da Aplicação
## Backend
    Spring Boot 
    Spring Data
    Java 11
    Maven
    JPA/Hibernate
    H2 Database
    Junit
    
## Frontend
    ReactJS
    Axios
    Babel
    Webpack
    Bootstrap

# Passos para execução
    **PRÉ-REQUISITOS**
    Instalar Java versão 8+ (preferencialmente Java 11)
    Instalar Node.js
    Intalar o Maven

## Executar backend

1.Clone o repositório em uma pasta de sua preferencia, no seu computador:
*`git clone git@gitlab.com:tabares.lopes/post-texto.git`

2.Navegue até a pasta raiz da aplicação backend:
*`cd post-texto/app-postagem/`

3.Construa a aplicação e execute os testes com o comando:
*`mvn install`

4.Execute a aplicação com o comando:
*`java -jar target/app-postagem-0.0.1-SNAPSHOT.jar`

5.Teste se a apliacação está em execução pelo browser:
*`http://localhost:8080/postagens`

    **Este comando deve retornar informações pré cadastradas na base de dados.
    
## Executar frontend
1.Na raiz da pasta que você clonou, navegue até a pasta 'frontend':
*`cd frontend/`

2.Instale as dependências da aplicação frontend:
*`npm install`

3.Execute a aplicação frontend:
*`npm run dev`

4.Você pode acessar a aplicação no browser de sua preferência pela URL:
`http://localhost:8081`






-
**Me encontro a disposição:**
tabares.lopes@gmail.com
(48)99836-2417
    

    