import React, { Component } from 'react'
import axios from 'axios'

import PageHeader from '../template/pageHeader'
import PostForm from './postForm'
import PostList from './postList'

const URL = 'http://localhost:8080/postagens'

export default class Post extends Component {
    constructor(props) {
        super(props)
        this.state = { title: '', description: '', list: [] }

        this.handleChangeTitle = this.handleChangeTitle.bind(this)
        this.handleChangeDescription = this.handleChangeDescription.bind(this)
        this.handleAdd = this.handleAdd.bind(this)
        this.handleUpvotes = this.handleUpvotes.bind(this)

        this.refresh();
    }

    refresh() {
        axios.get(URL).then(resp => this.setState({...this.state, title :  '', description : '', list : resp.data}));
    }

    handleChangeTitle(e) {
        this.setState({...this.state, title: e.target.value})
    }


    handleChangeDescription(e) {
        this.setState({...this.state, description: e.target.value})
    }
    
    handleAdd() {
        const title = this.state.title
        const description = this.state.description

        if(title.trim() == "" || description.trim() == "") {
            alert("Por favor, preencha os campos corretamente.");
        } else {
        
            axios.post(URL, {
                titulo : title,
                descricao : description 
            })
            .then(resp => console.log(resp.status))
            
        }

    }

    handleUpvotes(post) {
        axios.put(`${URL}/${post.id}`, {...post, upvotes: post.upvotes + 1})
        .then(resp => this.refresh())
    }
    
    render() {
        return (
            <div>
                <PageHeader name='Postagens' small='cadastro' />
                <PostForm 
                    title={this.state.title} 
                    description={this.state.description} 
                    handleAdd={this.handleAdd}
                    handleChangeTitle={this.handleChangeTitle}
                    handleChangeDescription={this.handleChangeDescription} />
                <PostList 
                    list={this.state.list}
                    handleUpvotes={this.handleUpvotes} />
            </div>
        )
    }  
}