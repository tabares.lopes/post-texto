import React from 'react'
import { Router, Route, Redirect, hashHistory } from 'react-router'

import About from '../about/about'
import Post from '../post/post'

export default props => (
    <Router history={hashHistory}>
        <Route path='/about' component={About} />
        <Route path='/post' component={Post} />
        <Redirect from='*' to='/post' />
    </Router>
)