package com.matheustabares.apppostagem.resources;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PostagemResourceTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private MockMvc mockMvc;
	
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}
	
	@Test
	public void testaListarComSucesso() throws Exception {
		String url = "/postagens";
		this.mockMvc.perform(get(url))
			.andExpect(status().isOk())
			.andExpect(content().string(containsString("Meu primeiro Post")))
			.andExpect(content().string(containsString("Meu segundo Post")))
			.andExpect(content().string(containsString("Meu terceiro Post")));
		
	}
	
	@Test
	public void testaInserirComSucesso() throws Exception {
		String url = "/postagens";
		this.mockMvc.perform(post(url)
			.content("{\"titulo\": \"Aplicação de Postagens\", \"descricao\": \"com upvotes para cada postagem\", \"upvotes\": 2}")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isCreated())
			.andExpect(header().string("Location", is("http://localhost/postagens/4")))
			.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	public void testaInserirComFalha() throws Exception {
		String url = "/postagens";
		this.mockMvc.perform(post(url)
			.content("{\"t1tulo\": \"Aplicação de Postagens\", \"descricao\": \"com upvotes para cada postagem\", \"upvotes\": 2}")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isBadRequest())
			.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	public void testaAtualizarUpvotesComSucesso() throws Exception {
		String url = "/postagens/1";
		this.mockMvc.perform(put(url)
		.content("{\"id\": 1, \"titulo\": \"Meu primeiro Post\", \"descricao\": \"Esse é o meu primeiro post\", \"upvotes\": 100}")
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isNoContent())
		.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	public void testaAtualizarUpvotesComFalha() throws Exception {
		String url = "/postagens/99";
		this.mockMvc.perform(put(url)
		.content("{\"id\": 99, \"titulo\": \"Aplicação de Postagens\", \"descricao\": \"com upvotes para cada postagem\", \"upvotes\": 3}")
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isNotFound())
		.andDo(MockMvcResultHandlers.print());
	}
}
