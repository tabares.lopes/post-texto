package com.matheustabares.apppostagem.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.matheustabares.apppostagem.domains.Postagem;

@Repository
public interface PostagemRepository extends JpaRepository<Postagem, Integer>{

}
