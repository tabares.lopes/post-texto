import React from 'react'
import Grid from '../template/grid'

export default props => (
    <div role='form' className='postForm'>
        <Grid cols='12 9 10'>
            
            <input id="title" className='form-control'
                placeholder='Adicione um título' maxLength='80' 
                onChange={props.handleChangeTitle}
                value={props.title}></input>&nbsp;
            
            <textarea id="description" className='form-control'
                placeholder='Adicione uma descrição' maxLength='300'
                onChange={props.handleChangeDescription}
                value={props.description}></textarea><br/>
            
            <button 
                className='btn btn-primary'
                disabled={props.title == '' || props.description == ''} 
                onClick={props.handleAdd} >
                Add
            </button><br/><br/>

        </Grid>

        
    </div>
)