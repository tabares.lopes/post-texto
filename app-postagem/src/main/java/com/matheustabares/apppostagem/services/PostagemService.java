package com.matheustabares.apppostagem.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matheustabares.apppostagem.domains.Postagem;
import com.matheustabares.apppostagem.repositories.PostagemRepository;

@Service
public class PostagemService {
	
	@Autowired
	private PostagemRepository repo;
	
	public List<Postagem> listar() {
		return repo.findAll();
	}

	public Postagem inserir(Postagem obj) {
		obj.setId(null);
		return repo.save(obj);
	}

	public Optional<Postagem> encontrarPorId(Integer id) {
		return repo.findById(id);
	}
	
	public Postagem atualizar(Postagem obj) {
		return repo.save(obj);
	}
	
}
