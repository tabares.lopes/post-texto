package com.matheustabares.apppostagem.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.matheustabares.apppostagem.domains.Postagem;
import com.matheustabares.apppostagem.repositories.PostagemRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PostagemRepositoryTest {
	
	@Autowired
	private PostagemRepository repo;
	
	@Test
	public void testaListarComSucesso() {
		List<Postagem> postagens = repo.findAll();
		assertThat(postagens.size()).isEqualTo(4);
	}
}
