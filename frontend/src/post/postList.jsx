import React from 'react'

export default props => {

    const renderRows = () => {
        const list = props.list || []
        return list.map(post => (
            <tr key={post.id}>
                <td>{post.titulo}</td>
                <td>{post.descricao}</td>
                <td>{post.upvotes}</td>
                <td>
                    <button className='btn btn-danger' onClick={() => props.handleUpvotes(post)}>
                        <i className='fa fa-plus'></i>
                    </button>
                </td>  
            </tr>
        ))
    }

    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Curtidas</th>
                    <th>Curtir</th>
                </tr>
            </thead>
            <tbody>
                {renderRows()}
            </tbody>
        </table>
    )
}